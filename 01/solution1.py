
file = open("./data/input","r")

sum = 0
frequencies = dict()
found = False
lines = file.readlines()
i = 0
while not found:
    i += 1
    for line in lines:
        x = int(line.rstrip())
        sum += x
        if frequencies.has_key(sum):
            print("First frequency {} found in run {}".format(sum,i))
            found = True    
            break        
        frequencies[sum] = 1

print("Sum = {}".format(sum))