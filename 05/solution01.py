file = open("./data/input","r")
lines = file.readlines()
input = lines[0].rstrip()

#input = "ABCDEFHAaABCDEFGH"

def something(s,lastRunIndex):
    if(lastRunIndex<1): 
        lastRunIndex = 1
    for x in range(lastRunIndex-1,len(s)-1):
        if( abs( ord(s[x]) - ord(s[x+1]) ) == 32 ):
            #print("Removing {}".format(s[x:x+2]))
            return s[:x] + s[x+2:], True, x
    return s, False, 1

import time
startTime = time.time()

index = 1
i = 0
while True:
    i += 1
    #print("Run {} from index {}, length of polymer is {}".format(i,index,len(input)))
    #print("Polymer {}".format(input))
    input, changeDone, index = something(input,index)
    if not changeDone:
        break        

print(input)
print(len(input))

print("Time: {} s".format(time.time()-startTime))