file = open("./data/input","r")
lines = file.readlines()
input = lines[0].rstrip()

#input = "ABCDEFHAaABCDEFGH"

def something(s,lastRunIndex):
    if(lastRunIndex<1): 
        lastRunIndex = 1
    for x in range(lastRunIndex-1,len(s)-1):
        if( abs( ord(s[x]) - ord(s[x+1]) ) == 32 ):
            #print("Removing {}".format(s[x:x+2]))
            return s[:x] + s[x+2:], True, x
    return s, False, 1

def findPolymer(polymer,c):
    polymer = polymer.replace(chr(c),"").replace(chr(c+32),"")
    index = 1
    i = 0
    while True:
        i += 1
        #print("Run {} from index {}, length of polymer is {}".format(i,index,len(input)))
        #print("Polymer {}".format(input))
        polymer, changeDone, index = something(polymer,index)
        if not changeDone:
            break        
    print("Length of reacted polymer is {} when unit {} is removed".format(len(polymer),chr(c)))

import time
wholeStartTime = time.time()

for c in range(ord("A"),ord("Z")+1):
    res = input.replace(chr(c),"").replace(chr(c+32),"")
    startTime = time.time()
    index = 1
    i = 0
    while True:
        i += 1
        #print("Run {} from index {}, length of polymer is {}".format(i,index,len(input)))
        #print("Polymer {}".format(input))
        res, changeDone, index = something(res,index)
        if not changeDone:
            break        
    print("Length of reacted polymer is {} when unit {} is removed".format(len(res),chr(c)))
    print("Time: {} s".format(time.time()-startTime))
print("Whole Time: {} s".format(time.time()-wholeStartTime))