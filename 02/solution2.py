from collections import Counter

file = open("./data/input","r")

lines = file.readlines()

for x in range(0,len(lines)): 
    firstWord = lines[x].rstrip()
    for y in range(0,len(lines)):
        if x == y:
            continue
        isEqual = True
        secondWord = lines[y].rstrip()
        wordDistance = len(firstWord)
        for z in range(0,len(firstWord)):
            if( firstWord[z] == secondWord[z] ):
                wordDistance -= 1
            else:
                atIndex = z
        if wordDistance == 1:
            print("Closest IDs are {} and {}".format(firstWord,secondWord))
            print("With removed character: {}".format(firstWord[:atIndex]+firstWord[atIndex+1:]))
            exit()