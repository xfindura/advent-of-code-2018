from collections import Counter

file = open("./data/input","r")

lines = file.readlines()

twos = 0
threes = 0

for line in lines: 
    counts = Counter(line.rstrip())
    twoFound = False
    threeFound = False
    for letter in counts:
        if( counts[letter] == 2 and not twoFound ):
            twos += 1
            twoFound = True
        if( counts[letter] == 3 and not threeFound ):
            threes += 1
            threeFound = True

    print(counts)

print("Checksum = {}".format(twos*threes))
