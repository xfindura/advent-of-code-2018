import re

class Piece:
    def __init__(self, id, startX, startY, sizeX, sizeY):
        self.id = int(id)
        self.posX = int(startX)
        self.posY = int(startY)
        self.sizeX = int(sizeX)
        self.sizeY = int(sizeY)

file = open("./data/input","r")

lines = file.readlines()

pieces = []
maxX = 0
maxY = 0

for line in lines:
    p = re.compile('#([0-9]+) @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)')
    m = p.match(line.rstrip())
    newPiece = Piece(m.group(1),m.group(2),m.group(3),m.group(4),m.group(5))
    if newPiece.posX + newPiece.sizeX > maxX:
        maxX = newPiece.posX + newPiece.sizeX
    if newPiece.posY + newPiece.sizeY > maxY:
        maxY = newPiece.posY + newPiece.sizeY
    pieces.append(newPiece)

import numpy

cloth = numpy.zeros((maxX,maxY))

for piece in pieces:
    for x in range(piece.posX,piece.posX+piece.sizeX):
        for y in range(piece.posY,piece.posY+piece.sizeY):
            cloth[x][y] += 1

sum = 0
for x in range(0,len(cloth)):
    for y in range(0,len(cloth[x])):
        if( cloth[x][y] > 1 ):
            sum += 1

print("Number of overlaping pieces: {}".format(sum))