import time
import datetime
import re

def getKey(item):
    return()

file = open("./data/input","r")
lines = file.readlines()

l = []

for line in lines:
    m = re.compile("\[([0-9- :]+)\] ([a-zA-Z #0-9]+)").match(line.rstrip())
    d = datetime.datetime.strptime("2018"+m.group(1)[4:],"%Y-%m-%d %H:%M").timetuple()
    l.append([time.strftime("%Y%m%d%H%M",d),m.group(2)])

l = sorted(l, key= lambda i: i[0])

import numpy

minutes = {}
sum = {}
for x in l:
    msg = x[1].split(" ")
    if( msg[0] == "Guard" ):
        currentGuard = int(msg[1][1:])
        minutes[currentGuard] = numpy.zeros(60)
        sum[currentGuard] = 0

for x in l:
    msg = x[1].split(" ")
    if( msg[0] == "Guard" ):
        currentGuard = int(msg[1][1:])
        print(currentGuard)
    if( msg[0] == "falls" ):
        startTime = int(x[0])
        print("start {}".format(startTime))
    if( msg[0] == "wakes" ):
        stopTime = int(x[0])
        print("stop {}".format(stopTime))
        y = stopTime-startTime
        print("asleep for {}".format(y))
        sum[currentGuard] += y
        startMinute = startTime % 100
        for minuteAsleep in range(startMinute,startMinute+y):
            minutes[currentGuard][minuteAsleep] += 1

print(sum)

s = sorted(sum,key=sum.get)

print(s)

for k in s:
    print("Guard #{} was {} minutes asleep".format(k,sum[k]))

sleepyGuard = s[len(s)-1]
ar = minutes[sleepyGuard]
mostMinute = numpy.where(ar == max(ar))[0][0]

print("Guard #{} slept most through minute {}, {} times, hash={}".format(sleepyGuard,mostMinute,max(ar),sleepyGuard*mostMinute))