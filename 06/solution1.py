import numpy

file = open("./data/input","r")
lines = file.readlines()

maxX = maxY = 0
points = []

for line in lines:
    line = line.rstrip()
    s = line.split(", ")
    points.append([int(s[0]),int(s[1])])
    maxX = max([maxX,int(s[0])])
    maxY = max([maxY,int(s[1])])

maxX += 2
maxY += 2

print("Size {} x {}".format(maxX,maxY))

field = numpy.full((3,maxX,maxY),numpy.inf)

for p in points:
    field[0][p[0]][p[1]] = points.index(p)
    field[1][p[0]][p[1]] = 0

for p in points:
    print("Couting for point {}".format(p))
    x = p[0]
    y = p[1]
    index = points.index(p)
    for mx in range(maxX):
        for my in range(maxY):
            dist = abs(mx-x)+abs(my-y)
            if( field[1][mx][my] > dist ):
                field[1][mx][my] = dist
                field[0][mx][my] = index
            if( field[2][mx][my] == numpy.inf ):
                field[2][mx][my] = dist
            else:
                field[2][mx][my] += dist

print(field)

sizes = [0] * len(points)

areaSize = 0
for mx in range(maxX):
        for my in range(maxY):
            p = int(field[0][mx][my])
            if( mx == 0 or my == 0 or mx == maxX-1 or my == maxY-1 ):
                sizes[p] = numpy.inf
            else:
                sizes[p] += 1
            if( field[2][mx][my] < 10000 ):
                areaSize += 1

maxSize = 0
for p in sizes:
    if ( p < numpy.inf ):
        maxSize = max([maxSize,p])

print("Max area ({}) is for point {}".format(maxSize,sizes.index(maxSize)))
print("Size of area within reach of all locations is {}".format(areaSize))
