file = open("./data/input","r")
lines = file.readlines()

rules = {}
previous = {}

for line in lines:
    m = line.rstrip().split(" ")
    currentStep = m[1]
    nextStep = m[7]

    if( not rules.has_key(currentStep) ):
        rules[currentStep] = []

    if( not rules.has_key(nextStep) ):
        rules[nextStep] = []
    
    if( not previous.has_key(nextStep) ):
        previous[nextStep] = []

    if( not previous.has_key(currentStep) ):
        previous[currentStep] = []

    rules[currentStep].append(nextStep)
    previous[nextStep].append(currentStep)

startStep = None
stepQueue = []
for r in previous:
    if not previous[r]:
        stepQueue.append(r)

print(rules)
print(previous)
print(startStep)

import numpy

def isWorking(w):
    for x in w:
        if x[0] != None:
            return True
    return False

workers = numpy.full((5,2),[None,0])
print(workers)

i = 0
done = []
timeSum = 0
while stepQueue or isWorking(workers):
    raw_input()
    stepQueue = sorted(stepQueue)
    print("Current queue: {}".format(stepQueue))
    
    minWork = numpy.inf
    for w in range(len(workers)):
        if workers[w][0] != None and workers[w][1] < minWork:
            minWork = workers[w][1]

    if minWork == numpy.inf:
        minWork = 0
    
    timeSum += minWork
    for w in range(len(workers)):
        if workers[w][0] != None:
            workers[w][1] -= minWork
            if workers[w][1] == 0:
                done.append(workers[w][0])
                stepQueue = list(set(stepQueue+rules[workers[w][0]]))
                workers[w][0] = None


    for w in range(len(workers)):
        if ( workers[w][0] != None and i == len(stepQueue) ):
            continue
        i = 0
        while i < len(stepQueue):
            step = stepQueue[i]
            #print("Checking previous steps for {}".format(step))
            cantDoNow = False

            for x in previous[step]:
                if( x not in done ):
                    #print("Step {} is not done yet, skipping".format(x))
                    cantDoNow = True
                    break

            if cantDoNow:
                i += 1
            else:
                print("Doing step {}".format(step))
                for w in range(len(workers)):
                    if workers[w][0] == None:
                        print("Assigning step {} to worker {}".format(step,w))
                        workers[w][0] = step
                        workers[w][1] = 60 + ord(step) - 64
                        stepQueue = stepQueue[:i] + stepQueue[i+1:]        
                        break
                else:
                    print("All workers working, can't assign step")

                i = 0
                print("----------------")

    for w in range(len(workers)):
        print("Worker {}: {}".format(w,workers[w]))
    print("Time Spent: {}".format(timeSum))
    print("----------------")


print(''.join(done))

    



